module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: {
        'hero': "url('/images/hero-crane.jpg')"
      },
      colors: {
        'primary': '#064460'
      },
      fontFamily: {
        'sans': ['"Proxima Soft"','sans-serif']
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
